import 'package:base_ui/home.dart';
import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text('Go home'),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    MyHomePage(title: 'Flutter Demo Home Page')));
            // Navigator.push(context,
            //     MaterialPageRoute(builder: (context) => Home2Screen()));
          },
        ),
      ),
    );
  }
}
